docker build -t roboops/nginx .
docker run -p 90:8080 -d -p 91:80 --name roboops roboops/nginx

//docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 roboops

Docker registry = roboops.azurecr.io

//TODO Should i use azure CI/CD Pipeline, i really don't want to get too deep into azure, as it might start costing me money. 

docker tag roboops/nginx roboops.azurecr.io/robo-ops_nginx
docker push roboops.azurecr.io/robo-ops_nginx
